<?php

namespace App\Controller;

use App\Entity\Categorie;
use App\Entity\Picture;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index(PaginatorInterface $paginator, Request $request): Response
    {   

        // Equivalent : SELECT * FROM pictures
        // findAll() retourne tous les résultats trouvés dans la table "pictures"
        // $resultesPictures = $this->getDoctrine()->getRepository(Picture::class)->findAll();

        // Equivalent : SELECT * FROM pictures WHERE is_validated = true
        $resultesPictures = $this->getDoctrine()->getRepository(Picture::class)->findBy(['is_validated' => true]);
        // Création du page par page
        // paginate() attend 3 arguments
        // 1. la collection de tous les résultats (pour nous, toutes les immages)
        // 2. La page sur laquelle nous sommes (càd : page 1, page 2...)
        // 3. Le nombre d'éléments par page
        
        $page = $request->query->getInt('page', 1);
        $pictures = $paginator->paginate(
            $resultesPictures,
            $request->query->getInt('page', 1), 
            10
        );

        // Sélectionne toutes les catégories
        $categories = $this->getDoctrine()->getRepository(Categorie::class)->findAll();

        return $this->render('home/index.html.twig', ['pictures' => $pictures,
        'categories' => $categories]);
    }

    /**
     * @Route("/category/{id}", name="picturesByCategory")
     */
    public function picturesByCategories($id, PaginatorInterface $paginator, Request $request)
    {   
        // dd veut dire Dump Data,
        // dd($id);

        // dump(), équivalent de var_dump()
        // Laisse la page se charger complètement et affiche le contenu du "dump" dans la debug bar.
        // dump($id);


        // find() sélectionne UN SEUL enregistrement selon son ID
        $category = $this->getDoctrine()->getRepository(Categorie::class)->find($id);
        $query = $this->getDoctrine()->getRepository(Picture::class)->findBy([
            'category' => $category,
            'is_validated' => true
        ]);

        if (!$category)
        {
            throw $this->createNotFoundException('La catégorie n\'existe pas');
        }

        // Page par page pour les images liées à la catégorie
        $page = $request->query->getInt('page', 1);
        $pictures = $paginator->paginate(
            // $category->getPictures(),
            $query,
            $page === 0 ? 1 : $page,
            10
        );

        return $this->render('home/picturesByCategory.html.twig', ['category' => $category, 'pictures' => $pictures]);
    }

    /**
     * "requirements" permet de valider le type de la donnée
     * @Route("/show/picture/{id}", name="show_picture", requirements={"id"="\d+"})
     */
    public function showPicture($id, PaginatorInterface $paginator, Request $request)
    {

        $picture = $this->getDoctrine()->getRepository(Picture::class)->find($id);
        $pictures = $this->getDoctrine()->getRepository(Picture::class)->findBy([
            'category' => $picture->getCategory(),
            'is_validated' => true
        ]);

        if (!$picture)
        {
            throw $this->createNotFoundException('Cette image n\'existe pas');
        }

        // Page par page pour les images
        $page = $request->query->getInt('page', 1);
        $photos = $paginator->paginate(
            // $picture->getCategory()->getPictures(),
            $pictures,
            $page === 0 ? 1 : $page,
            10
        );

        return $this->render('home/showPicture.html.twig', [
            'picture' => $picture,
            'photos' => $photos
        ]);
    }
}
