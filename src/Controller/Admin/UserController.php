<?php

namespace App\Controller\Admin;

use App\Entity\User;
use App\Form\UserType;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends AbstractController
{
    /**
     * @Route("/admin/users", name="admin_users")
     */
    public function index(PaginatorInterface $paginator, Request $request): Response
    {
        $query = $this->getDoctrine()->getRepository(User::class)->findAll();

        $page = $request->query->getInt('page', 1);
        $users = $paginator->paginate(
            $query,
            $page === 0 ? 1 : $page,
            10
        );            
        return $this->render('user/index.html.twig', [
            'users' => $users
        ]);
    }

     /**
     * @Route("/admin/users/delete/{id}", name="admin_users_delete")
     */
    public function delete($id)
    {
        $user = $this->getDoctrine()->getRepository(User::class)->find($id);

        if (!$user) {
            throw $this->createNotFoundException('Cette utilisateur n\'existe pas'); // Erreur 404 - Ressource non trouvée
            
        }


        $doctrine = $this->getDoctrine()->getManager();
        $doctrine->remove($user);
        $doctrine->flush();

        $this->addFlash('success', 'L\'utilisateur à bien été supprimée');

        return $this->redirectToRoute('admin_users');
    }

    /**
     * @Route("/admin/users/edit/{id}", name="admin_users_edit")
     */
    public function edit($id, Request $request)
    {
        // Sélectionner un enregistrement en BDD
        $user = $this->getDoctrine()->getRepository(User::class)->find($id);

        // Si l'image n'existe pas, on retourne une erreur 404
        // Ou si l'image n'appartient pas à l'utilisateur connecté, on retourne une erreur 404C
        if (!$user) {
            throw $this->createNotFoundException('Cette utilisateur n\'existe pas'); // Erreur 404 - Ressource non trouvée
            
        }


        $formEditUser = $this->createForm(UserType::class, $user, [
            'validation_groups' => 'edit_user'
        ]);
        $formEditUser->handleRequest($request);

        if ($formEditUser->isSubmitted() && $formEditUser->isValid()) {

            $doctrine = $this->getDoctrine()->getManager();
            $doctrine->persist($user);
            $doctrine->flush();

            $this->addFlash('success', 'Les modifications ont bien été enregistrées');

            return $this->redirectToRoute('admin_users');
        }

        return $this->render('user/edit.html.twig', [
            'formEditUser' => $formEditUser->createView()
        ]);
    }
}
