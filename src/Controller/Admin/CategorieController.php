<?php

namespace App\Controller\Admin;

use App\Entity\Categorie;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\AsciiSlugger;
use App\Form\CategoryType;


class CategorieController extends AbstractController
{
    /**
     * @Route("/admin", name="admin")
     */
    public function index(PaginatorInterface $paginator, Request $request): Response
    {

        $query = $this->getDoctrine()->getRepository(Categorie::class)->findAll();

        $page = $request->query->getInt('page', 1);
        $categories = $paginator->paginate(
            $query,
            $page === 0 ? 1 : $page,
            20
        );

        return $this->render('admin/index.html.twig', [
            'categories' => $categories
        ]);
    }

    /**
     * @Route("/admin/categories/new", name="admin_categories_new")
     */

    public function new(Request $request)
    {
        // Instanciation de l'entité désirée
        $category = new Categorie();

        // Création du formulaire
        $formNewCategory = $this->createForm(CategoryType::class, $category);
        $formNewCategory->handleRequest($request);

        // Traitement des données si le formulaire est envoyé et que les validations sont acceptées
        if ($formNewCategory->isSubmitted() && $formNewCategory->isValid()) {
            // Transforme le nom de la catégorie en slug
            $category->setSlug((new AsciiSlugger())->slug(strtolower($category->getName())));

            // Inertion en BDD
            $doctrine = $this->getDoctrine()->getManager();
            $doctrine->persist($category);
            $doctrine->flush();

            // Message flash
            $this->addFlash('success', 'La catégorie');

            // Retour à mon tableau de catégorie
            return $this->redirectToRoute('admin');
        }

        return $this->render('admin/new.html.twig', [
            'formNewCategory' => $formNewCategory->createView()
        ]);
    }

    /**
     * @Route("/admin/categories/edit/{id}", name="admin_categories_edit")
     */
    public function edit($id, Request $request)
    {
        $category = $this->getDoctrine()->getRepository(Categorie::class)->find($id);

        // Erreur 404 si la catégorie n'éxiste pas
        if (!$category) {
            throw $this->createNotFoundException('Cette catégorie n\'existe pas');
        }

        // Création du formulaire
        $formEditCategory = $this->createForm(CategoryType::class, $category);
        $formEditCategory->handleRequest($request);

        // Vérifie que le formulaire soit envoyé et validé
        if ($formEditCategory->isSubmitted() && $formEditCategory->isValid()) {
            // Transforme le nom de la catégorie en slug
            $category->setSlug((new AsciiSlugger())->slug(strtolower($category->getName())));

            // Mise à jour en BDD
            $doctrine = $this->getDoctrine()->getManager();
            $doctrine->persist($category);
            $doctrine->flush();

            // Message flash
            $this->addFlash('success', 'La catégorie à bien été modifiée');

            // Retour sur le Listing des catégories
            return $this->redirectToRoute('admin');
        }

        return $this->render('admin/edit.html.twig', [
            'formEditCategory' => $formEditCategory->createView()
        ]);
    }
    /**
     * @Route("/admin/categories/delete/{id}", name="admin_categories_delete")
     */
    public function delete($id)
    {
        $category = $this->getDoctrine()->getRepository(Categorie::class)->find($id);

        if (!$category) {
            throw $this->createNotFoundException('La catégorie n\'existe pas');
        }

        $doctrine = $this->getDoctrine()->getManager();
        $doctrine->remove($category);
        $doctrine->flush();

        $this->addFlash('success', 'La suppression de la catégorie s\'est bien déroulé');

        return $this->redirectToRoute('admin');
    }
}
