<?php

namespace App\Controller;

use App\Entity\Picture;
use App\Form\UploadPictureType;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PictureController extends AbstractController
{
    /**
     * @Route("/admin/pictures", name="admin_picture")
     */
    public function index(PaginatorInterface $paginator, Request $request): Response
    {

        $query = $this->getDoctrine()->getRepository(Picture::class)->findAll();

        $page = $request->query->getInt('page', 1);
        $pictures = $paginator->paginate(
            $query,
            $page === 0 ? 1 : $page,
            30
        );

        return $this->render('picture/index.html.twig', [
            'pictures' => $pictures
        ]);
    }

    /**
     * @Route("/admin/pictures/delete/{id}", name="admin_pictures_delete")
     */
    public function delete($id)
    {
        $picture = $this->getDoctrine()->getRepository(Picture::class)->find($id);

        // Si l'image n'existe pas, on retourne une erreur 404
        // Ou si l'image n'appartient pas à l'utilisateur connecté, on retourne une erreur 404C
        if (!$picture) {
            throw $this->createNotFoundException('Cette image n\'existe pas'); // Erreur 404 - Ressource non trouvée
            // throw $this->createAccessDeniedException('Cette image n\'existe pas'); // Erreur 403 - Accès refusé
        }


        $doctrine = $this->getDoctrine()->getManager();
        $doctrine->remove($picture);
        $doctrine->flush();

        $this->addFlash('success', 'La photo à bien été supprimée');

        return $this->redirectToRoute('admin_picture');
    }

    /**
     * @Route("/admin/pictures/edit/{id}", name="admin_pictures_edit")
     */
    public function edit($id, Request $request)
    {
        $picture = $this->getDoctrine()->getRepository(Picture::class)->find($id);

        if (!$picture) {
            throw $this->createNotFoundException('La photo n\'existe pas');
        }

        $formEditPicture = $this->createForm(UploadPictureType::class, $picture, [
            'validation_groups' => 'edit_picture'
        ]);
        $formEditPicture->handleRequest($request);

        if ($formEditPicture->isSubmitted() && $formEditPicture->isValid()) {
            $picture->setCreatedAt(new \DateTimeImmutable());

            $doctrine = $this->getDoctrine()->getManager();
            $doctrine->persist($picture);
            $doctrine->flush();

            $this->addFlash('success', 'La photo a bien été modifié');
            
            return $this->redirectToRoute('admin_picture');
        }     

        return $this->render('picture/edit.html.twig', [
            'formEditPicture' => $formEditPicture->createView()
        ]);
    }
}
