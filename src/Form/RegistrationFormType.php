<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\IsTrue;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Email;

class RegistrationFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstname', TextType::class, [
                'required' => true, // Champs obligatoire en ajoutant l'attribut "required"
                'label' => 'Prénom', // Choix du nom du label, si vous en voulez pas, mettez la valeur à "false"
                'attr' => [
                    'placeholder' => 'Veuillez saisir un prénom',
                    // 'class' => 'nameClassCss deuxiemeClassCss', // Config pour ajouter des classes CSS  au champs in
                ],
                'constraints' => [ // Contraintes de validations. Vérification des données côté serveur.
                    new NotBlank([
                        'message' => 'Veuillez saisir un prénom'
                    ])
                ],
            ])
            ->add('lastname') // Pour firstname et email : label et des contraintes de validations
            ->add('email', EmailType::class, [
                'required' => true,
                'label' => 'Adresse e-mail',
                'constraints' => [
                    new NotBlank([
                        'message' => 'Veuillez saisir une adresse e-mail'
                    ]),
                    new Email([
                        'message' => 'Votre adresse e-mail est invalide'
                    ])
                ]
            ]) // Vérifier la validité de l'email
            ->add('agreeTerms', CheckboxType::class, [
                'mapped' => false, // Spécifie que ce champ n'est pas relié à l'entité en cours
                'label' => 'En soumettant ce formulaire, j\'accepte que MONSITE conserve mes données personnelles via ces formulaire. Aucune exploitation
                commerciale ne sera faite des données conservées.',

                'constraints' => [
                    new IsTrue([
                        'message' => 'You should agree to our terms.',
                    ]),
                ],
            ])
            ->add('plainPassword', RepeatedType::class, [
                'type' => PasswordType::class,
                'invalid_message' => 'Les mots de passe ne correspond pas',
                'first_options' => ['label' => 'Mot de passe'],
                'second_options' => ['label' => 'Confimer le mot de passe'],
                'required' => true,
                // instead of being set onto the object directly,
                // this is read and encoded in the controller
                'mapped' => false,
                'constraints' => [
                    new NotBlank([
                        'message' => 'Please enter a password',
                    ]),
                    new Length([
                        'min' => 6,
                        'minMessage' => 'Votre mot de passe doit contenir {{ limit }} caractères minimum',
                        // max length allowed by Symfony for security reasons
                        'max' => 4096,
                    ]),
                ],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
